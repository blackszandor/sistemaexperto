/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sistemaexperto.motor;

/**
 *
 * @author L440
 */
public class DiagnosticoController 
{
    public static void Run()
    {        
        ConsoleView zooView             = new ConsoleView();
        MotorController motorController = new MotorController();        
        EventHandler eventController    = new EventHandler(zooView);
        
        zooView.setMotorController(motorController);
        
        motorController.addEscuchador(eventController);        
        motorController.ejecutar();    
    }
}
