/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sistemaexperto.motor;

import java.util.logging.Level;
import java.util.logging.Logger;
import jess.JessEvent;
import jess.JessException;
import jess.Rete;

/**
 *
 * @author L440
 */
public class MotorController 
{
    Rete Motor = null;
    
    public MotorController()
    {
        try 
        {
            this.Motor = new Rete();            
            this.Motor.reset();
            this.Motor.batch("sistemaexperto/clips/diagnostico.clp");
            //motor.run();
        } catch (JessException ex) 
        {
            Logger.getLogger(MotorController.class.getName()).log(Level.SEVERE, null, ex);
        }    
    }    
    
    public void afirmar(String hecho) throws JessException{
        this.Motor.assertString(hecho);
        this.Motor.run();
    }
    
    public void addEscuchador(EventHandler eventController){
        this.Motor.addJessListener(eventController);
        this.Motor.setEventMask(JessEvent.DEFRULE_FIRED);
    }
    
    public void ejecutar(){
        try {
            this.Motor.run();
        } catch (JessException ex) 
        {
            Logger.getLogger(MotorController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }   
    
}
