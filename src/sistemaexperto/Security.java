/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sistemaexperto;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.ResultSet;

import sistemaexperto.User;

/**
 *
 * @author Moisés Rangel Narváez;
 */
public class Security 
{
    private String username = null;
    private String password = null;
    private String profile  = null;
    private String fullname = null;
    
    public Security(String username, String password) 
    {
        this.username = username;
        this.password = password;
    }
    
    public User getUser()
    {
        User U = new User(this.fullname, this.profile);
        return U;
    }
    
    public Security Login() throws ClassNotFoundException, SQLException, InstantiationException, IllegalAccessException, Exception 
    {
        Connection conn = null;
        Statement stmt  = null;
        ResultSet rs    = null;   
        String Pass     = null;
        
        try
        {
           Class.forName("com.mysql.jdbc.Driver").newInstance();
           conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/sistemaexperto?user=root&password=mysql");
           
           stmt = conn.createStatement();
           
           String Qry = "SELECT username, password, name, profile FROM Usuarios WHERE username = '" + this.username +"'";
           
           rs = stmt.executeQuery(Qry);     
           rs = stmt.getResultSet();
           
            int count = 0;
            while (rs.next()) 
            {
                Pass          = rs.getString("password");
                this.fullname = rs.getString("name");
                this.profile  = rs.getString("profile");
                ++count;
            }           
            
            if(count == 0)
            {
                throw new Exception("No existe el nombre de usuario.");
            }
            
            if(!this.password.equals(Pass))
            {
                throw new Exception("La contraseña es incorrecta.");
            }
            
        }catch (SQLException ex) 
        {           
            System.out.println(ex.getMessage());
        }        
        finally 
        {
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException sqlEx) { } // ignore

                rs = null;
            }

            if (stmt != null) {
                try {
                    stmt.close();
                } catch (SQLException sqlEx) { } // ignore

                stmt = null;
            }
        }        
        return this;
    }
}

