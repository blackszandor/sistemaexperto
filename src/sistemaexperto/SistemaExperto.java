/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sistemaexperto;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.sql.SQLException;
import sistemaexperto.motor.DiagnosticoController;

/**
 *
 * @author L440
 */
public class SistemaExperto 
{
    /**
     * @param args the command line arguments
     * @throws java.io.IOException
     * @throws java.lang.ClassNotFoundException
     * @throws java.sql.SQLException
     * @throws java.lang.InstantiationException
     * @throws java.lang.IllegalAccessException
     */
    public static void main(String[] args) throws IOException, ClassNotFoundException, SQLException, InstantiationException, IllegalAccessException, Exception 
    {        
        int value = -1;

        while( value < 0)
        {
            try
            {
                BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
                System.out.print("Ingrese su nombre de usuario: ");        
                String username = br.readLine();

                System.out.print("Ingrese su contraseña: ");
                String password = br.readLine();        

                Security Auth = new Security(username.trim(), password.trim());
                User user     = Auth.Login().getUser();

                System.out.println("Bienvenido " + user.Name + " al sistema de consultas.");
                System.out.println("Perfil: " + user.Profile);
                value = 0;
                               
                
            }catch(Exception e)
            {
                System.out.println(e.getMessage());
            }     
        }    
        
        
        if(value == 0)
        {
            DiagnosticoController.Run();     
        }
    }   
}